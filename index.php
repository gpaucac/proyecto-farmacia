
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Login</title>
    <link rel="icon" href="../../../../favicon.ico">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/signin.css" rel="stylesheet">
  </head>

  <body class="text-center">
    <form class="form-signin" action="validar.php" method="post">
      <img class="mb-4" src="images/perfil.jpg" alt="" width="150" height="150">
      <h1 class="h3 mb-3 font-weight-normal">Por favor inicie sesión</h1>
      <label for="usuario_f" class="sr-only">Usuario</label>
      <input type="text" id="usuario_f" name="usuario_f" class="form-control" placeholder="Usuario" required autofocus>
      <label for="contrasena_f" class="sr-only">Contraseña</label>
      <input type="password" id="contrasena_f" name="contrasena_f" class="form-control" placeholder="Contraseña" required>

      <button class="btn btn-lg btn-primary btn-block" type="submit">Iniciar Sesion</button>
      
    </form>
  </body>
</html>
