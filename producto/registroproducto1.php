<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8"/>
	<meta http-equiv="Content-type" content="text/html"; charset="utf-8" />
	<meta name="viewport" content= "width = device-width, user-scalable = no, initial-scale = 1.0, maximun-scale = 1.0, minimum-scale= 1.0"> 
    <link rel="stylesheet" href="../css/bootstrap.min.css">

	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" >
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" ></script>

    <link rel="stylesheet" href="../css/navbar-fixed-left.min.css">
	<title>Producto</title>
</head>
<body>

	<?php 
		include '../navbarleft2.php';
	?>
	<?php
	 	date_default_timezone_set('America/Lima');
		 $fecha_actual=date("Y-m-d");
	 ?>

	<div class="container"><br><br>
		
		<h2 style="text-transform:uppercase" >Productos</h2><br><br>
		
		<form method="post" action="registroproducto2.php" autocomplete="off">

			<!-- Button trigger modal -->
			<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalCenter">Agregar Producto +</button>

			<!-- Modal -->
			<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			<div class="modal-dialog modal-dialog-centered" role="document">
				<div class="modal-content">
					
				<div class="modal-header">
					<h3 class="modal-title" id="exampleModalLongTitle">PRODUCTOS</h3>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<div class="modal-body">
					<div class="row">
						
						<div class="form-group col-md-4">
							<label>fecha:</label> 
							<input class="form-control" type="text" name="producto_f" value="<?= $fecha_actual ?>" readonly>
						</div>
						<div class="form-group col-md-4">
							<label>producto:</label> 
							<input class="form-control" type="text" name="producto_f" placeholder="Ejm: panadol" required>
						</div>
						<div class="form-group col-md-4">
							<label for="">concentracion:</label> 
							<input class="form-control" type="text" name="concentracion_f" placeholder="Ejm: 30ml" required>
						</div>
						<div class="form-group col-md-4">
							<label>cantidad:</label> 
							<input class="form-control" type="number" name="cantidad_f" required>
						</div>
						<div class="form-group col-md-4">
							<label>formula:</label> 
							<input class="form-control" type="text" name="formula_f" required>
						</div>
						<div class="form-group col-md-4">
							<label>lote:</label> 
							<input class="form-control" type="text" name="lote_f" required>
						</div>
						<div class="form-group col-md-4">
							<label>registro sanitario:</label> 
							<input class="form-control" type="text" name="registrosanitario_f" required>
						</div>
						<div class="form-group col-md-4">
							<label>laboratorio:</label> 
							<input class="form-control" type="text" name="laboratorio_f" required>
						</div>
						<div class="form-group col-md-4">
							<label>fecha vencimiento:</label> 
							<input class="form-control" type="text" name="fechavencimiento_f" required>
						</div>
						<div class="form-group col-md-4">
							<label>precio registro:</label> 
							<input class="form-control" type="number" name="precio1_f" required>
						</div>
						<div class="form-group col-md-4">
							<label>precio venta:</label> 
							<input class="form-control" type="text" name="precio2_f" required>
						</div>				
					</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Cancelar</button>
					<input class="btn btn-success" type="submit" value="Agregar">
				</div>
				</div>
			</div>
			</div>
			
		</form>

	</div>
	<br>
	
	<?php 
		include("listaproducto.php")
	 ?>

	 <script src="../js/jquery-3.3.1.min.js"></script>
	 <script src="../js/bootstrap.min.js"></script>
	 <script src="../js/bootstrap-hover-dropdown.min.js"></script>

</body>
</html>