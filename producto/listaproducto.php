<!DOCTYPE html>
<html lang="en">

<script>
	function confirmar(url){
		if(!confirm("¿Esta seguro que desea eliminar?")){
			return false;
		}else{
			document.location=url;
			return true;
		}
	}	
</script>

	<div class="container">
	<div class="table-responsive">

		<?php 
			require '../connect_db.php';

			$registros=mysqli_query($con, "select*from producto2") or die (mysqli_error($con));

			echo '<table class="table table-striped table-bordered table-hover">';
			echo '<tr> <td>Numero</td> <td style="text-transform:capitalize">
			Descripcion</td><td>
			Concentracion</td><td>
			Cantidad</td><td>
			Formula</td><td>
			Lote</td><td>
			Registro Sanitario</td><td>
			Laboratorio</td><td>
			Fecha Vencimiento</td><td>
			Precio Registro</td><td>
			Precio Venta</td><td>
			Eliminar</td><td>
			Modificar</td></tr> ';

			$i=1;

			while($reg=mysqli_fetch_array($registros)){

				echo '<tr>';

				echo '<td>';
				echo $i++;
				echo '</td>';

				echo '<td>';
				echo $reg['descripcionproducto'];
				echo '</td>';

				echo '<td>';
				echo $reg['concentracionproducto'];
				echo '</td>';

				echo '<td>';
				echo $reg['cantidadproducto'];
				echo '</td>';

				echo '<td>';
				echo $reg['formulaproducto'];
				echo '</td>';

				echo '<td>';
				echo $reg['loteproducto'];
				echo '</td>';

				echo '<td>';
				echo $reg['registrosanitarioproducto'];
				echo '</td>';

				echo '<td>';
				echo $reg['laboratorioproducto'];
				echo '</td>';

				echo '<td>';
				echo $reg['fechavenproducto'];
				echo '</td>';

				echo '<td>';
				echo $reg['precio1producto'];
				echo '</td>';

				echo '<td>';
				echo $reg['precio2producto'];
				echo '</td>';

				echo '<td>';
				?>

					<a href="javascript:;" onclick="confirmar('eliminarproducto.php? idproducto_f=<?php echo $reg['idproducto'];?>'); 
					return false;">Eliminar</a>

				<?php
					echo '</td>';
					
					echo '<td>';
					echo '<a href="modificarproducto1.php? idproducto_f='.$reg['idproducto'].'">Modificar</a>';
										
					echo '</td>';

					echo '</tr>';

			}

			echo '</table>';

			mysqli_close($con);
		 ?>
	</div>
	</div>
<!-- </body>
</html> -->

