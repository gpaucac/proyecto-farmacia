<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-type" content="text/html"; charset=utf-8" />
	<meta name="viewport" content= "width = device-width, user-scalable = no, initial-scale = 1.0, maximun-scale = 1.0, minimum-scale= 1.0"> 
    <link rel="stylesheet" href="../css/bootstrap.min.css">
    <link rel="stylesheet" href="../css/navbar-fixed-left.min.css">
	<title>Modificacion producto</title>
</head>
<body>
	<?php include ("../navbarleft2.php"); ?>
	<?php 
		require '../connect_db.php';
		$registro=mysqli_query($con, "select*from producto2 where idproducto='$_REQUEST[idproducto_f]'");
		if($reg=mysqli_fetch_array($registro))
		{
	 ?>
	
	<div class="container"><br><br><br>
		
		<h2 style="text-transform:uppercase" >Modificar Producto</h2> <br><br><br>
		
		<form action="modificarproducto2.php" autocomplete="off">
			
			<div class="row">				
				
				<div class="form-group col-md-4">
					<label>producto:</label> 
					<input class="form-control" type="text" name="descripcionproducto_f" value="<?php echo $reg['descripcionproducto']; ?>">
				</div>
				<div class="form-group col-md-4">
					<label>concentracion:</label> 
					<input class="form-control" type="text" name="concentracionproducto_f" value="<?php echo $reg['concentracionproducto']; ?>">
				</div>
				<div class="form-group col-md-4">
					<label>cantidad:</label> 
					<input class="form-control" type="number" name="cantidadproducto_f" value="<?php echo $reg['cantidadproducto']; ?>">
				</div>
				<div class="form-group col-md-4">
					<label>formula:</label> 
					<input class="form-control" type="text" name="formulaproducto_f" value="<?php echo $reg['formulaproducto']; ?>">
				</div>
				<div class="form-group col-md-4">
					<label>lote:</label> 
					<input class="form-control" type="text" name="loteproducto_f" value="<?php echo $reg['loteproducto']; ?>">
				</div>
				<div class="form-group col-md-4">
					<label>registro sanitario:</label> 
					<input class="form-control" type="text" name="registrosanitarioproducto_f" value="<?php echo $reg['registrosanitarioproducto']; ?>">
				</div>
				<div class="form-group col-md-4">
					<label>laboratorio:</label> 
					<input class="form-control" type="text" name="laboratorioproducto_f" value="<?php echo $reg['laboratorioproducto']; ?>">
				</div>
				<div class="form-group col-md-4">
					<label>fecha vencimiento:</label> 
					<input class="form-control" type="text" name="fechavenproducto_f" value="<?php echo $reg['fechavenproducto']; ?>">
				</div>
				<div class="form-group col-md-4">
					<label>precio registro:</label> 
					<input class="form-control" type="number" name="precio1producto_f" value="<?php echo $reg['precio1producto']; ?>">
				</div>
				<div class="form-group col-md-4">
					<label>precio venta:</label> 
					<input class="form-control" type="text" name="precio2producto_f" value="<?php echo $reg['precio2producto']; ?>">
				</div>
					
			</div>
						
			<input class="btn btn-primary" type="submit" value="Modificar">

			<!-- oculto -->
			<input type="hidden" name="idproducto_f" value="<?php echo $_REQUEST['idproducto_f']; ?>">
			


		</form>
		<?php
 	}
	 else
	 echo 'No existe un artículo con dicho código';
	 mysqli_close($con);

 ?>
	</div>
	<br>

	<?php 
	//	include("listaproducto.php")
	 ?>

	<script src="../js/jquery-3.3.1.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	<script src="../js/bootstrap-hover-dropdown.min.js"></script>


</body>
</html>

