-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 21-06-2021 a las 21:49:20
-- Versión del servidor: 10.4.19-MariaDB
-- Versión de PHP: 8.0.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bellesti`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `almacen`
--

CREATE TABLE `almacen` (
  `id` int(10) NOT NULL,
  `stock` int(10) NOT NULL,
  `codigo_producto_alma` int(10) NOT NULL,
  `descripcion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `almacen`
--

INSERT INTO `almacen` (`id`, `stock`, `codigo_producto_alma`, `descripcion`) VALUES
(1, 0, 1, ''),
(2, 5, 2, ''),
(3, 100, 2, ''),
(4, 100, 2, ''),
(5, 1000, 3, ''),
(6, 2000, 4, ''),
(7, 10, 1, ''),
(8, 100, 1, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idcliente` int(11) NOT NULL,
  `codigocliente` int(30) NOT NULL,
  `descripcioncliente` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idcliente`, `codigocliente`, `descripcioncliente`) VALUES
(18, 0, 'Juan Quispe'),
(19, 0, 'Frank Bolivar'),
(22, 0, 'Alex Llallaque'),
(23, 0, 'Hernan Cama'),
(24, 0, 'Vanesa Sulla');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compraventa`
--

CREATE TABLE `compraventa` (
  `codigocompraventa` int(10) NOT NULL,
  `numerodocumento` int(10) NOT NULL,
  `compraoventa` varchar(50) NOT NULL,
  `tipodocumento` varchar(20) NOT NULL,
  `fecharegistro` date NOT NULL,
  `producto` varchar(100) NOT NULL,
  `cantidad` int(10) NOT NULL,
  `precio` decimal(10,2) NOT NULL,
  `total` decimal(10,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `compraventa`
--

INSERT INTO `compraventa` (`codigocompraventa`, `numerodocumento`, `compraoventa`, `tipodocumento`, `fecharegistro`, `producto`, `cantidad`, `precio`, `total`) VALUES
(1, 0, 'venta', 'boleta', '2018-09-11', 'Crema Antiacne', 10, '2.00', '20.00'),
(2, 0, 'compra', 'factura', '2018-09-10', 'Locion', 20, '4.00', '80.00'),
(3, 5, 'compra', '', '2000-12-12', 'Crema Hidratante 2', 1000, '4.00', '4000.00'),
(4, 5, 'compra', '2', '2000-12-12', 'Crema Hidratante 2', 1000, '4.00', '4000.00'),
(5, 6, 'venta', '3', '2000-12-12', 'Crema Hidratante 2', 12, '1.00', '12.00'),
(6, 12, 'compra', '2', '2000-12-12', 'Crema Hidratante 12', 12, '1.00', '12.00'),
(7, 6, 'venta', '3', '2000-12-12', 'Crema Hidratante 2', 12, '1.00', '12.00'),
(8, 13, 'venta', '2', '2000-12-12', 'Crema Hidratante13', 100, '3.00', '300.00'),
(9, 4, 'venta', '2', '2018-09-11', 'Crema Hidratante', 100, '3.00', '0.00'),
(10, 35, 'venta', '3', '2018-09-11', 'Crema Hidratante 35', 100, '3.00', '300.00'),
(11, 41, '', 'boleta', '2018-09-12', 'Crema Hidratante 41', 2, '4.50', '9.00'),
(12, 42, 'compra', 'boleta', '2018-09-12', 'Crema Hidratante 42', 100, '3.40', '340.00'),
(13, 0, 'venta', 'boleta', '2018-09-12', 'crema prueba', 1, '45.00', '45.00'),
(14, 123, 'compra', 'factura', '2018-09-11', 'crema prueba', 10, '40.00', '400.00'),
(15, 4, 'compra', 'boleta', '2018-09-14', 'Crema Hidratante 01', 100, '3.00', '300.00'),
(16, 4, 'compra', 'boleta', '2018-09-17', 'Crema Hidratante004', 12, '3.00', '36.00'),
(17, 5, 'compra', 'boleta', '2018-09-17', 'Crema Hidratante005', 12, '4.20', '50.40'),
(18, 6, 'compra', 'boleta', '2018-09-17', 'Crema Hidratante006', 12, '3.60', '43.20'),
(19, 0, '', '', '0000-00-00', '', 0, '0.00', '0.00'),
(20, 7, 'compra', 'boleta', '2018-09-17', 'Crema Hidratante007', 14, '3.60', '50.40'),
(21, 7, 'compra', 'boleta', '2018-09-17', 'Crema Hidratante007', 14, '3.60', '50.40'),
(22, 6565, 'compra', 'boleta', '2018-09-16', 'po09', 70, '5.00', '350.00'),
(23, 6565, 'compra', 'boleta', '2018-09-17', 'po09', 70, '5.00', '350.00'),
(24, 12, 'compra', 'boleta', '2018-09-26', 'Crema Hidratante 164', 100, '3.00', '300.00'),
(25, 13, 'venta', 'guia de remision', '2018-09-27', 'Crema Hidratante 165', 6, '36.00', '216.00'),
(26, 4, 'compra', 'boleta', '2018-09-27', 'Crema Hidratante 200', 100, '3.00', '300.00'),
(27, 201, 'compra', 'factura', '2018-09-27', 'Crema Hidratante 201', 100, '3.00', '300.00'),
(28, 178, 'compra', 'boleta', '2018-10-03', 'Crema Hidratante 178', 12, '4.50', '54.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factor1`
--

CREATE TABLE `factor1` (
  `numero1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `factor1`
--

INSERT INTO `factor1` (`numero1`) VALUES
(1),
(2),
(3),
(4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factor2`
--

CREATE TABLE `factor2` (
  `numero2` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `factor2`
--

INSERT INTO `factor2` (`numero2`) VALUES
(10),
(20),
(30),
(40);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `familia`
--

CREATE TABLE `familia` (
  `idfamilia` int(11) NOT NULL,
  `descripcionfamilia` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `familia`
--

INSERT INTO `familia` (`idfamilia`, `descripcionfamilia`) VALUES
(1, 'Cosmeticos'),
(3, 'Limpieza Facial'),
(4, 'Belleza');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `laboratorio`
--

CREATE TABLE `laboratorio` (
  `idlaboratorio` int(11) NOT NULL,
  `descripcionlaboratorio` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `laboratorio`
--

INSERT INTO `laboratorio` (`idlaboratorio`, `descripcionlaboratorio`) VALUES
(2, 'Portugal'),
(3, 'Natura'),
(6, 'Esika');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `marca`
--

CREATE TABLE `marca` (
  `idmarca` int(11) NOT NULL,
  `descripcionmarca` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `marca`
--

INSERT INTO `marca` (`idmarca`, `descripcionmarca`) VALUES
(1, 'Panadol'),
(6, 'Asepsia'),
(9, 'Bioderma'),
(10, 'EKOS'),
(14, 'Gentlecleanse'),
(15, 'Ego'),
(22, 'Jhonson'),
(23, 'camay'),
(25, 'Sedal'),
(26, 'Andorra'),
(27, 'PerÃº'),
(28, 'Ecuador'),
(29, 'Venezuela'),
(30, 'Argentina'),
(31, 'AO'),
(32, 'CanadÃ¡'),
(33, 'AntÃ¡rtida'),
(34, 'Angola'),
(35, 'AfganistÃ¡n'),
(36, 'AfganistÃ¡n'),
(37, 'Antillas Holandesas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idproducto` int(255) NOT NULL,
  `descripcionproducto` varchar(100) NOT NULL,
  `marca` varchar(20) NOT NULL,
  `familia` varchar(20) NOT NULL,
  `laboratorio` varchar(20) NOT NULL,
  `unidad` varchar(20) NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `precio1` decimal(10,0) NOT NULL,
  `precio2` decimal(10,0) NOT NULL,
  `precio3` decimal(10,0) NOT NULL,
  `precio4` decimal(10,0) NOT NULL,
  `stock` int(11) NOT NULL,
  `codigoproducto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idproducto`, `descripcionproducto`, `marca`, `familia`, `laboratorio`, `unidad`, `fecha_vencimiento`, `precio1`, `precio2`, `precio3`, `precio4`, `stock`, `codigoproducto`) VALUES
(1, 'GENTLECLEANSE5', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '150ml', '2030-12-12', '0', '0', '0', '0', 0, 'COD00001'),
(2, 'CREAM CLEANSE', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '250ml', '2030-12-12', '61', '71', '90', '95', 0, 'COD00002'),
(3, 'HYDR8 NIGHT CREAM', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '50ml', '2030-12-12', '113', '131', '170', '175', 0, 'COD00003'),
(4, 'EYE CLEANSE', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '100ml', '2030-12-12', '46', '53', '55', '65', 0, 'COD00004'),
(5, 'C- TETRA CREAM', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '50ml', '2030-12-12', '104', '125', '160', '165', 0, 'COD00005'),
(6, 'RETINOL 1TR CREAM', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '50ml', '2030-12-12', '89', '106', '135', '140', 0, 'COD00006'),
(7, 'DARK CIRCLES', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '15ml', '2030-12-12', '110', '128', '155', '160', 0, 'COD00007'),
(8, 'r- RETINOATE', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '50ml', '2030-12-12', '313', '375', '460', '480', 0, 'COD00008'),
(9, 'BETA CLEANSE', 'Sin Definir', 'MEDIK8 - Pieles gras', 'PROSKIN', '150ml', '2030-12-12', '69', '82', '104', '109', 0, 'COD00009'),
(10, 'BETA GEL ', 'Sin Definir', 'MEDIK8 - Pieles gras', 'PROSKIN', '15ml', '2030-12-12', '96', '115', '145', '152', 0, 'COD00010'),
(11, 'BETA MOISTURISE', 'Sin Definir', 'MEDIK8 - Pieles gras', 'PROSKIN', '50ml', '2030-12-12', '112', '130', '160', '165', 0, 'COD00011'),
(12, 'RED ALERT CLEANSE', 'Sin Definir', 'MEDIK8 - Pieles Sens', 'PROSKIN', '150ml', '2030-12-12', '0', '0', '0', '0', 0, 'COD00012'),
(13, 'REDNESS CORRECTOR', 'Sin Definir', 'MEDIK8 - Pieles Sens', 'PROSKIN', '50ml', '2030-12-12', '162', '189', '205', '215', 0, 'COD00013'),
(14, 'WHITE BALANCE CLICK OXY-R', 'Sin Definir', 'MEDIK8 - Pigmentacio', 'PROSKIN', '2x10ml', '2030-12-12', '0', '0', '0', '0', 0, 'COD00014'),
(15, 'HYDR8 B5 SERUM', 'Sin Definir', 'MEDIK8 - Hidratante', 'PROSKIN', '30ml', '2030-12-12', '122', '142', '180', '190', 0, 'COD00015'),
(16, 'GLOW OIL', 'Sin Definir', 'MEDIK8 - Hidratante', 'PROSKIN', '30ml', '2030-12-12', '126', '148', '180', '190', 0, 'COD00016'),
(17, 'HYDR8 MANOS', 'Sin Definir', 'MEDIK8 - Hidratante', 'PROSKIN', '60ml', '2030-12-12', '72', '86', '95', '99', 0, 'COD00017'),
(18, 'LOPECIAN SHAMPOO', 'Sin Definir', 'LINEA CAPILAR', 'PROSKIN', '250ml', '2030-12-12', '88', '105', '128', '135', 0, 'COD00018'),
(19, 'PIROCTOL AS SHAMPOO', 'Sin Definir', 'LINEA CAPILAR', 'PROSKIN', '120ml', '2030-12-12', '49', '57', '75', '75', 0, 'COD00019'),
(20, 'BTSES FOCAL ACTION GEL (Inhibidor de arrugas)', 'Sin Definir', 'ANTIARRUGAS', 'SESDERMA', '15ml', '2030-12-12', '43', '66', '78', '83', 0, 'COD00020'),
(21, 'BTSES SERUM HIDRATANTE ANTIARRUGAS', 'Sin Definir', 'ANTIARRUGAS', 'SESDERMA', '30ml', '2030-12-12', '78', '120', '142', '150', 0, 'COD00021'),
(22, 'FILLDERMA ONE 50 ML. (Crema rellenadora)', 'Sin Definir', 'ANTIARRUGAS', 'SESDERMA', '50ml', '2030-12-12', '83', '128', '152', '160', 0, 'COD00022'),
(23, 'SESGEN 32 CREMA ACTIVADORA CELULAR', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '50ml', '2030-12-12', '105', '136', '155', '160', 0, 'COD00023'),
(24, 'SESGEN 32 SERUM ACTIVADOR CELULAR', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '30ml', '2030-12-12', '111', '144', '145', '165', 0, 'COD00024'),
(25, 'FACTOR G CREMA GEL REJUVENECEDOR', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '50ml', '2030-12-12', '89', '136', '162', '171', 0, 'COD00025'),
(26, 'FACTOR G CREMA REGENERADORA', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '50ml', '2030-12-12', '105', '130', '147', '155', 0, 'COD00026'),
(27, 'FACTOR G SERUM DE BURBUJAS LIPIDICAS', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '30ml', '2030-12-12', '111', '144', '145', '165', 0, 'COD00027'),
(28, 'RETI AGE CONTORNO DE OJOS ANTIAGING', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '15ml', '2030-12-12', '81', '106', '115', '125', 0, 'COD00028'),
(29, 'RETI AGE CREMA FACIAL ANTIAGING', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '50ml', '2030-12-12', '105', '136', '147', '155', 0, 'COD00029'),
(30, 'RETI AGE SERUM ANTIAGING', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '30ml', '2030-12-12', '111', '144', '145', '165', 0, 'COD00030'),
(31, 'C -VIT EYE CONTOUR CREAM 15 ML.', 'Sin Definir', 'ANTIOXIDANTES', 'SESDERMA', '15ml', '2030-12-12', '0', '0', '90', '100', 0, 'COD00031'),
(32, 'C-VIT CC CREAM SPF 15  30 ML.', 'Sin Definir', 'ANTIOXIDANTES', 'SESDERMA', '30ml', '2030-12-12', '72', '86', '100', '110', 0, 'COD00032'),
(33, 'RESVERADERM ANTIOX DNA REPAIR 50 ML.', 'Sin Definir', 'ANTIOXIDANTES', 'SESDERMA', '50ml', '2030-12-12', '98', '128', '150', '160', 0, 'COD00033'),
(34, 'RESVERADERM SERUM LIPOSOMADO', 'Sin Definir', 'ANTIOXIDANTES', 'SESDERMA', '30ml', '2030-12-12', '102', '160', '165', '0', 0, 'COD00034'),
(35, 'SESLASH KIT (SESLASH + GLICARE 15ml)', 'Sin Definir', 'CUIDADO DEL CONTORNO', 'SESDERMA', 'pack', '2030-12-12', '108', '112', '165', '180', 0, 'COD00035'),
(36, 'AZELAC RU LIPOSOMAL SERUM', 'Sin Definir', 'DESPIGMENTANTES', 'SESDERMA', '30ml', '2030-12-12', '0', '0', '0', '0', 0, 'COD00036'),
(37, 'REPASKIN DRY TOUCH (TOQUE SECO) SPF 50', 'Sin Definir', 'FOTOREPARACION', 'SESDERMA', '50 ml', '2030-12-12', '60', '72', '90', '100', 0, 'COD00037'),
(38, 'REPASKIN SLIK TOUCH (TACTO SEDA)SPF 50', 'Sin Definir', 'FOTOREPARACION', 'SESDERMA', '50ml', '2030-12-12', '68', '88', '100', '105', 0, 'COD00038'),
(39, 'HIDRADERM HYAL LIPOSOMAL SERUM ', 'Sin Definir', 'HIDRATACION', 'SESDERMA', '30ml', '2030-12-12', '78', '119', '143', '150', 0, 'COD00039'),
(40, 'HIDRADERM CLEANSING MILK 200 ML.', 'Sin Definir', 'EXFOLIACION Y LIMPIE', 'SESDERMA', '200ml', '2030-12-12', '54', '64', '75', '80', 0, 'COD00040'),
(41, 'AZELAC MOISTURIZING GEL  (GEL HIDRATANTE)50 ML.', 'Sin Definir', 'ROSACEA Y ACNE', 'SESDERMA', '50ml', '2030-12-12', '87', '104', '110', '120', 0, 'COD00041'),
(42, 'SALISES FOCAL TREATMENT 15 ML. ', 'Sin Definir', 'ROSACEA Y ACNE', 'SESDERMA', '15ml ', '2030-12-12', '39', '60', '71', '75', 0, 'COD00042'),
(43, 'SALISES MOISTURIZING GEL (GEL HIDRATANTE) 50 ML.', 'Sin Definir', 'ROSACEA Y ACNE', 'SESDERMA', '50ml', '2030-12-12', '89', '124', '147', '155', 0, 'COD00043'),
(44, 'KIT LUMINOSIDAD (C-VIT liposomal serum + C-VIT crema gel revitalizante)', 'Sin Definir', 'PACK PROMOCION', 'SESDERMA', 'pack (2 und)', '2030-12-12', '102', '136', '180', '190', 0, 'COD00044'),
(45, 'KIT C-VIT LUZ EN TU PIEL (Glowing fluid + eye contour cream)', 'Sin Definir', 'PACK PROMOCION', 'SESDERMA', 'pack (2 und)', '2030-12-12', '75', '112', '160', '170', 0, 'COD00045'),
(46, 'FOTO PROTECTOR SUN BRUSH MINERAL  FPS 50+ (Polvo Compacto)', 'Sin Definir', 'FACIALES', 'SESDERMA', '4gr', '2030-12-12', '64', '84', '95', '99', 0, 'COD00046'),
(47, 'FOTO ULTRA 100 ACTIVE UNIFY FUSION FLUID COLOR SPF 50+', 'Sin Definir', 'FACIALES', 'SESDERMA', '50ml', '2030-12-12', '78', '117', '110', '115', 0, 'COD00047'),
(48, 'FOTO ULTRA 100 SPOT PREVENT FUSION FLUID SPF 50+', 'Sin Definir', 'FACIALES', 'SESDERMA', '50ml', '2030-12-12', '75', '112', '105', '110', 0, 'COD00048'),
(49, 'FOTO PROTECTOR FUSION WATER  FPS 50+', 'Sin Definir', 'FACIALES', 'SESDERMA', '50ml', '2030-12-12', '66', '86', '95', '99', 0, 'COD00049'),
(50, 'ISDINCEUTICS FLAVO-C ( serum antioxidante con vitamina c y ginkgo biloba', 'Sin Definir', 'ISDINCEUTICS', 'SESDERMA', '30ml', '2030-12-12', '175', '175', '245', '249', 0, 'COD00050'),
(51, 'ISDINCEUTICS MELACLEAR (serum corrector unificador del tono)', 'Sin Definir', 'ISDINCEUTICS', 'SESDERMA', '15ml', '2030-12-12', '122', '122', '170', '174', 0, 'COD00051'),
(52, 'ISDINCEUTICS SKIN DROPS SAND (maquillaje fluido de cobertura adaptable)', 'Sin Definir', 'ISDINCEUTICS', 'SESDERMA', '15ml', '2030-12-12', '140', '140', '195', '199', 0, 'COD00052'),
(53, 'ISDINCEUTICS SKIN DROPS BRONZE  (maquillaje fluido de cobertura adaptable)', 'Sin Definir', 'ISDINCEUTICS', 'SESDERMA', '15ml', '2030-12-12', '140', '140', '195', '199', 0, 'COD00053'),
(54, 'ISDINCEUTICS K-OX EYES (crema contorno de ojos reduce bolsas y ojeras)', 'Sin Definir', 'ISDINCEUTICS', 'SESDERMA', '15gr', '2030-12-12', '129', '129', '180', '184', 0, 'COD00054'),
(55, 'FOAMING GLYCOLIC WASH', 'Sin Definir', 'NEOSTRATA', 'MEDSTYLE', 'Fco x 100ml', '2030-12-12', '75', '87', '100', '110', 0, 'COD00055'),
(56, 'OIL CONTROL GEL', 'Sin Definir', 'NEOSTRATA', 'MEDSTYLE', 'Fco x 30ml', '2030-12-12', '79', '68', '95', '100', 0, 'COD00056'),
(57, 'SKIN ACTIVE TRIPLE FIRMING NECK CREAM', 'Sin Definir', 'NEOSTRATA', 'MEDSTYLE', 'Fco x 75gr', '2030-12-12', '102', '127', '155', '160', 0, 'COD00057'),
(58, 'SKIN ACTIVE MATRIX SUPPORT SPF 30+', 'Sin Definir', 'NEOSTRATA', 'MEDSTYLE', 'Tubo x 50gr', '2030-12-12', '102', '127', '150', '160', 0, 'COD00058'),
(59, 'SKIN ACTIVE CELLULAR RESTORATION ', 'Sin Definir', 'NEOSTRATA', 'MEDSTYLE', 'Tubo x 50gr', '2030-12-12', '102', '127', '150', '160', 0, 'COD00059'),
(60, 'SKIN ACTIVE INTENSIVE EYE THERAPY', 'Sin Definir', 'NEOSTRATA', 'MEDSTYLE', 'Fco x 15gr', '2030-12-12', '95', '119', '140', '150', 0, 'COD00060'),
(61, 'SKIN ACTIVE EXFOLIATING WASH (Syner G Formula 8.5) 125 ML.', 'Sin Definir', 'NEOSTRATA', 'MEDSTYLE', 'Fco x 125ml', '2030-12-12', '83', '103', '120', '130', 0, 'COD00061'),
(62, 'SODERMIX', 'Sin Definir', 'LSI', 'MEDSTYLE', 'Tubo x 30gr', '2030-12-12', '79', '87', '100', '105', 0, 'COD00062'),
(63, 'POSTOPIX', 'Sin Definir', 'LSI', 'MEDSTYLE', 'Tubo x 15gr', '2030-12-12', '40', '56', '90', '100', 0, 'COD00063'),
(64, 'MD LASH FACTOR (Eye lash conditiones) ', 'Sin Definir', 'MD', 'MEDSTYLE', 'Tubo x 5.9ml', '2030-12-12', '300', '360', '390', '410', 0, 'COD00064'),
(65, 'COMPACTO SPF 50+', 'Sin Definir', 'MEDBLOCK', 'MEDSTYLE', '10gr', '2030-12-12', '47', '66', '80', '85', 0, 'COD00065'),
(66, 'CREMA COLOR SPF 50+', 'Sin Definir', 'MEDBLOCK', 'MEDSTYLE', 'Tubo x 80gr', '2030-12-12', '38', '55', '70', '75', 0, 'COD00066'),
(67, 'ATODERM INTENSIVE GEL MOUSSANT', 'Sin Definir', 'ATODERM', 'BIODERMA', '200ml', '2030-12-12', '0', '38', '60', '60', 0, 'COD00067'),
(68, 'ATODERM INTENSIVE BAUME', 'Sin Definir', 'ATODERM', 'BIODERMA', '200ml', '2030-12-12', '0', '71', '100', '110', 0, 'COD00068'),
(69, 'CICABIO CREMA 40 ML. ', 'Sin Definir', 'CICABIO', 'BIODERMA', '40ml', '2030-12-12', '45', '53', '76', '83', 0, 'COD00069'),
(70, 'HYDRABIO SERUM', 'Sin Definir', 'HYDRABIO', 'BIODERMA', '40ml', '2030-12-12', '0', '0', '0', '0', 0, 'COD00070'),
(71, 'PHOTODERM AKN MAT SPF 30 FLUIDE MATIFIANT', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '40ml', '2030-12-12', '61', '71', '110', '114', 0, 'COD00071'),
(72, 'PHOTODERM AKN SPF 30 SPRAY 100 ML. ', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '100ml', '2030-12-12', '0', '82', '110', '112', 0, 'COD00072'),
(73, 'PHOTODERM AR SPF 50+ TINTED CREAM NATURAL COLOUR', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '30ml', '2030-12-12', '61', '71', '110', '114', 0, 'COD00073'),
(74, 'PHOTODERM M SPF 50+ TINTED PROTECTIVE CREAM GOLDEN COLOUR', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '40ml', '2030-12-12', '61', '71', '110', '112', 0, 'COD00074'),
(75, 'PHOTODERM MAX SPF 50+ AQUA FLUIDE TEINTE LIGHT COLOUR', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '40ml', '2030-12-12', '0', '77', '105', '111', 0, 'COD00075'),
(76, 'PHOTODERM MAX SPF 50+ AQUA FLUIDE TEINTE GOLDEN COLOUR', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '40ml', '2030-12-12', '0', '77', '105', '111', 0, 'COD00076'),
(77, 'PHOTODERM NUDE TOUCH SPF 50+ NATURAL COLOUR', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '40ml', '2030-12-12', '67', '78', '115', '118', 0, 'COD00077'),
(78, 'PHOTODERM NUDE TOUCH SPF 50+ LIGHT COLOUR', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '40ml', '2030-12-12', '67', '78', '115', '118', 0, 'COD00078'),
(79, 'PHOTODERM NUDE TOUCH SPF 50+ GOLDEN  COLOUR', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '40ml', '2030-12-12', '67', '78', '115', '118', 0, 'COD00079'),
(80, 'S?BIUM GEL MOUSSANT 500 ML.', 'Sin Definir', 'SEBIUM', 'BIODERMA', '500ml', '2030-12-12', '82', '96', '125', '130', 0, 'COD00080'),
(81, 'SEBIUM H2O SOLUTION MICELLAIRE 250 ML. ', 'Sin Definir', 'SEBIUM', 'BIODERMA', '250ml', '2030-12-12', '0', '52', '70', '75', 0, 'COD00081'),
(82, 'SEBIUM GLOBAL 30 ML.', 'Sin Definir', 'SEBIUM', 'BIODERMA', '30ml', '2030-12-12', '56', '56', '86', '90', 0, 'COD00082'),
(83, 'SEBIUM GLOBAL COVER (Teinte Universal)', 'Sin Definir', 'SEBIUM', 'BIODERMA', '30ml', '2030-12-12', '51', '59', '90', '95', 0, 'COD00083'),
(84, 'SENSIBIO AR SOIN ANTI - ROUGEURS 40ML.', 'Sin Definir', 'SENSIBIO', 'BIODERMA', '40ml', '2030-12-12', '0', '63', '95', '101', 0, 'COD00084'),
(85, 'SENSIBIO CONTORNO DE OJOS', 'Sin Definir', 'SENSIBIO', 'BIODERMA', '15ml', '2030-12-12', '0', '0', '0', '0', 0, 'COD00085'),
(86, 'SENSIBIO BB CREAM AR (clair light)', 'Sin Definir', 'SENSIBIO', 'BIODERMA', '40ml', '2030-12-12', '0', '94', '105', '110', 0, 'COD00086'),
(87, 'SENSIBIO H2O 100 ML. ', 'Sin Definir', 'SENSIBIO', 'BIODERMA', '100ml', '2030-12-12', '33', '39', '50', '52', 0, 'COD00087'),
(88, 'SENSIBIO H2O 500 ML. ', 'Sin Definir', 'SENSIBIO', 'BIODERMA', '250ml', '2030-12-12', '82', '73', '120', '124', 0, 'COD00088'),
(89, 'SENSIBIO GEL MOUSSANT 200 ML. ', 'Sin Definir', 'SENSIBIO', 'BIODERMA', '200ml', '2030-12-12', '50', '58', '83', '88', 0, 'COD00089'),
(90, 'PACK SENSIBIO BB CREAM AR (clair light) + ESPONJA DE APLICACI?N', 'Sin Definir', 'SENSIBIO', 'BIODERMA', 'pack', '2030-12-12', '0', '80', '120', '123', 0, 'COD00090'),
(91, 'KIT HIDRABIO (HIDRABIO SERUM 40 ml +SENSIBIO H2O SOLUTION MICELLAR 100ml+ SENSIBIO EYE CONTOUR 15ml)', 'Sin Definir', 'SENSIBIO', 'BIODERMA', 'pack', '2030-12-12', '0', '200', '219', '230', 0, 'COD00091'),
(92, 'PACK POST-PROCEDIMIENTO DERMATOLOGICO (Cicabio creme 40ml+Photoderm M spf 50+ golden colour 40ml', 'Sin Definir', 'SENSIBIO', 'BIODERMA', 'pack', '2030-12-12', '0', '96', '0', '0', 0, 'COD00092'),
(93, 'TECNOSKIN LIPBOOST GLOSS COLOR (Hyaluronic acid)', 'Sin Definir', 'FISSION LAB', 'FISION LAB', '7ml', '2030-12-12', '46', '50', '57', '60', 0, 'COD00093'),
(94, 'TECNOSKIN LIPBOOST GLOSS WHITE (Hyaluronic acid)', 'Sin Definir', 'FISSION LAB', 'FISION LAB', '7ml', '2030-12-12', '46', '50', '57', '60', 0, 'COD00094'),
(95, 'STRATAMED (Tratamiento de Cicatrices)', 'Sin Definir', 'FISSION LAB', 'FISION LAB', '10gr', '2030-12-12', '95', '114', '124', '130', 0, 'COD00095'),
(96, 'AMINOTER MAX SHAMPOO', 'Sin Definir', 'LAFAGE', 'LAFAGE', '300ml', '2030-12-12', '69', '76', '100', '110', 0, 'COD00096'),
(97, 'AMINOTER (Suplemento dietario) 30 Capsulas x caja', 'Sin Definir', 'LAFAGE', 'LAFAGE', 'cap 30', '2030-12-12', '0', '161', '235', '239', 0, 'COD00097'),
(98, 'C-VIT CREMA HIDRO PROTECTORA (PIEL NORMAL Y SECA) ', 'Sin Definir', 'CVITAL', 'ATACHE (EUROLABS)', '50ml', '2030-12-12', '0', '69', '140', '150', 0, 'COD00098'),
(99, 'C-VIT CREMA HIDRO PROTECTORA (PIELGRASA Y MIXTA) ', 'Sin Definir', 'CVITAL', 'ATACHE (EUROLABS)', '50ml', '2030-12-12', '0', '69', '140', '150', 0, 'COD00099'),
(100, 'INTENSIVE LIFT CONTOUR (contorno de ojos y labios) ', 'Sin Definir', 'LIFT THERAPY', 'ATACHE (EUROLABS)', '15ml', '2030-12-12', '0', '129', '170', '180', 0, 'COD00100'),
(101, 'PERFORMANCE NECK SOLUTION (REAFIRMANTE DE CUELLO Y PAPADA) ', 'Sin Definir', 'LIFT THERAPY', 'ATACHE (EUROLABS)', '50ml', '2030-12-12', '0', '175', '220', '230', 0, 'COD00101'),
(102, 'PERFORMANCE SOLUTION (SERUM ACLARADOR EPID?RMICO)  ', 'Sin Definir', 'DESPIGMEN', 'ATACHE (EUROLABS)', '50ml', '2030-12-12', '0', '175', '220', '230', 0, 'COD00102'),
(103, 'SPECIFIC ANTI-TACHES SOLUTION (CREMA DESPIGMENTANTE DERMICO) ', 'Sin Definir', 'DESPIGMEN', 'ATACHE (EUROLABS)', '15ml', '2030-12-12', '0', '99', '128', '135', 0, 'COD00103'),
(104, 'UMBRELLA BASE COMPACTA SPF 50+ TONO OSCURO', 'Sin Definir', 'UMBRELLA', 'ROEMMERS', '11g', '2030-12-12', '83', '83', '91', '100', 0, 'COD00104'),
(105, 'UMBRELLA BASE COMPACTA SPF 50+ TONO CLARO', 'Sin Definir', 'UMBRELLA', 'ROEMMERS', '11g', '2030-12-12', '83', '83', '91', '100', 0, 'COD00105'),
(106, 'UMBRELLA GEL SPF 50+ (piel normal a grasa)', 'Sin Definir', 'UMBRELLA', 'ROEMMERS', '60gr', '2030-12-12', '73', '73', '90', '95', 0, 'COD00106'),
(107, 'RESVERAX CREMA FACIAL ANTIAGE 30 G.', 'Sin Definir', 'HIDRISAGE', 'ROEMMERS', '30gr', '2030-12-12', '75', '75', '137', '140', 0, 'COD00107'),
(108, 'PLIANCE BASE COMPACTA TONO CLARO 11 G.', 'Sin Definir', 'HIDRISAGE', 'ROEMMERS', '11gr', '2030-12-12', '76', '76', '135', '140', 0, 'COD00108'),
(109, 'AQUAPRIM CREMA HIDRATANTE 30G. ', 'Sin Definir', 'HIDRISAGE', 'ROEMMERS', '30gr', '2030-12-12', '75', '75', '137', '140', 0, 'COD00109'),
(110, 'LUMED EMULGEL (Aclarante despigmentante) 40 G. PROTECTOR SOLAR SPF 50+', 'Sin Definir', 'HIDRISAGE', 'ROEMMERS', '40gr', '2030-12-12', '0', '50', '102', '110', 0, 'COD00110'),
(111, 'LUMED BASE FLUIDA (Aclarante despigmentante)PROTECTOR SOLAR SPF 50+ TONO CLARO 40 G.', 'Sin Definir', 'HIDRISAGE', 'ROEMMERS', '40gr', '2030-12-12', '83', '83', '145', '150', 0, 'COD00111'),
(112, 'LUMED BASE FLUIDA (Aclarante despigmentante)PROTECTOR SOLAR SPF 50+ TONO OSCURO 40 G.', 'Sin Definir', 'HIDRISAGE', 'ROEMMERS', '40gr', '2030-12-12', '83', '83', '150', '154', 0, 'COD00112'),
(113, 'CAVIAR LUXURY (CONTORNO DE OJOS)', 'Sin Definir', 'LE LAB', 'LE LAB', '30gr.', '2030-12-12', '0', '0', '100', '120', 0, 'COD00113'),
(114, 'CAVIAR LUXURY CREMA', 'Sin Definir', 'LE LAB', 'LE LAB', '50gr.', '2030-12-12', '0', '0', '150', '160', 0, 'COD00114'),
(115, 'CREMA RESTAURADORA ESPECIAL DE MANOS', 'Sin Definir', 'LE LAB', 'LE LAB', '50gr.', '2030-12-12', '0', '0', '0', '0', 0, 'COD00115'),
(116, 'GLOBAL PERFECTION CREMA (CARA CUELLO Y ESCOTE)', 'Sin Definir', 'LE LAB', 'LE LAB', '50gr', '2030-12-12', '80', '100', '0', '0', 0, 'COD00116'),
(117, 'MICRO ESFERAS DE VITAMINA C EN GEL DE ACIDO HIALURONICO ', 'Sin Definir', 'LE LAB', 'LE LAB', '50gr.', '2030-12-12', '70', '90', '130', '140', 0, 'COD00117'),
(118, 'MOLECULAR EXPERT YOUNGER SKIN', 'Sin Definir', 'LE LAB', 'LE LAB', '50gr.', '2030-12-12', '80', '100', '0', '0', 0, 'COD00118'),
(119, 'BLANCHE EFFECT CREMA DE NOCHE', 'Sin Definir', 'LE LAB', 'LE LAB', '30gr', '2030-12-12', '0', '0', '0', '0', 0, 'COD00119'),
(120, 'CREMA DESPIGMENTANTE CON AC. TRANEXAMICO', 'Sin Definir', 'LE LAB', 'LE LAB', '30ng', '2030-12-12', '36', '56', '0', '0', 0, 'COD00120'),
(121, 'BEBE GEL DERMOLIMPIADOR', 'Sin Definir', 'ALPHANOVA', 'ALPHANOVA', '200ml', '2030-12-12', '0', '0', '70', '80', 0, 'COD00121'),
(122, 'BEBE COLD CREAM', 'Sin Definir', 'ALPHANOVA', 'ALPHANOVA', '50gr', '2030-12-12', '0', '0', '70', '80', 0, 'COD00122'),
(123, 'SUN MILK (BLOQUEADOR)', 'Sin Definir', 'ALPHANOVA', 'ALPHANOVA', '50gr', '2030-12-12', '0', '0', '0', '0', 0, 'COD00123'),
(124, 'FEQOXIL BUCODISPENSABLE (PASTILLAS)', 'Sin Definir', 'OTROS', 'OTROS', 'cap 30', '2030-12-12', '0', '0', '0', '0', 0, 'COD00124'),
(125, 'TONICO ASTRINGENTE', 'Sin Definir', 'EXEL', 'EXCEL', '0', '2030-12-12', '0', '0', '50', '60', 0, 'COD00125'),
(126, 'TONICO COLAGENO ', 'Sin Definir', 'EXEL', 'EXCEL', '0', '2030-12-12', '0', '0', '50', '60', 0, 'COD00126'),
(127, '', '', '', '', '', '0000-00-00', '0', '0', '0', '0', 0, ''),
(128, '', '', '', '', '', '0000-00-00', '0', '0', '0', '0', 0, ''),
(129, 'paracetamol', '', '', '', '', '0000-00-00', '0', '0', '0', '0', 0, ''),
(130, 'paracetamol', '', '', '', '', '0000-00-00', '0', '0', '0', '0', 0, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto2`
--

CREATE TABLE `producto2` (
  `idproducto` int(11) NOT NULL,
  `descripcionproducto` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `concentracionproducto` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `formulaproducto` varchar(80) COLLATE utf8_unicode_ci DEFAULT NULL,
  `cantidadproducto` int(11) DEFAULT NULL,
  `loteproducto` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fechavenproducto` date DEFAULT NULL,
  `registrosanitarioproducto` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `laboratorioproducto` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `precio1producto` decimal(20,2) DEFAULT NULL,
  `precio2producto` decimal(20,2) DEFAULT NULL,
  `fecharegproducto` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `producto2`
--

INSERT INTO `producto2` (`idproducto`, `descripcionproducto`, `concentracionproducto`, `formulaproducto`, `cantidadproducto`, `loteproducto`, `fechavenproducto`, `registrosanitarioproducto`, `laboratorioproducto`, `precio1producto`, `precio2producto`, `fecharegproducto`) VALUES
(1, 'Amoxiciloina', '500mg', 'Tableta', 23, '00001', '2023-12-22', 'EE-01', 'Portugal', '12.23', '13.23', '2020-06-27'),
(2, 'pracetamol', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'apronax', '50ml', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `idproducto` int(255) NOT NULL,
  `descripcionproducto` varchar(100) NOT NULL,
  `marca` varchar(20) NOT NULL,
  `familia` varchar(20) NOT NULL,
  `laboratorio` varchar(20) NOT NULL,
  `unidad` varchar(20) NOT NULL,
  `fecha_vencimiento` date NOT NULL,
  `precio1` decimal(10,0) NOT NULL,
  `precio2` decimal(10,0) NOT NULL,
  `precio3` decimal(10,0) NOT NULL,
  `precio4` decimal(10,0) NOT NULL,
  `stock` int(11) NOT NULL,
  `codigoproducto` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`idproducto`, `descripcionproducto`, `marca`, `familia`, `laboratorio`, `unidad`, `fecha_vencimiento`, `precio1`, `precio2`, `precio3`, `precio4`, `stock`, `codigoproducto`) VALUES
(1, 'GENTLECLEANSE9', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '150ml', '2030-12-12', '0', '0', '0', '0', 0, 'COD00001'),
(2, 'CREAM CLEANSE', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '250ml', '2030-12-12', '61', '71', '90', '95', 0, 'COD00002'),
(3, 'HYDR8 NIGHT CREAM', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '50ml', '2030-12-12', '113', '131', '170', '175', 0, 'COD00003'),
(4, 'EYE CLEANSE', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '100ml', '2030-12-12', '46', '53', '55', '65', 0, 'COD00004'),
(5, 'C- TETRA CREAM', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '50ml', '2030-12-12', '104', '125', '160', '165', 0, 'COD00005'),
(6, 'RETINOL 1TR CREAM', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '50ml', '2030-12-12', '89', '106', '135', '140', 0, 'COD00006'),
(7, 'DARK CIRCLES', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '15ml', '2030-12-12', '110', '128', '155', '160', 0, 'COD00007'),
(8, 'r- RETINOATE', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '50ml', '2030-12-12', '313', '375', '460', '480', 0, 'COD00008'),
(9, 'BETA CLEANSE', 'Sin Definir', 'MEDIK8 - Pieles gras', 'PROSKIN', '150ml', '2030-12-12', '69', '82', '104', '109', 0, 'COD00009'),
(10, 'BETA GEL ', 'Sin Definir', 'MEDIK8 - Pieles gras', 'PROSKIN', '15ml', '2030-12-12', '96', '115', '145', '152', 0, 'COD00010'),
(11, 'BETA MOISTURISE', 'Sin Definir', 'MEDIK8 - Pieles gras', 'PROSKIN', '50ml', '2030-12-12', '112', '130', '160', '165', 0, 'COD00011'),
(12, 'RED ALERT CLEANSE', 'Sin Definir', 'MEDIK8 - Pieles Sens', 'PROSKIN', '150ml', '2030-12-12', '0', '0', '0', '0', 0, 'COD00012'),
(13, 'REDNESS CORRECTOR', 'Sin Definir', 'MEDIK8 - Pieles Sens', 'PROSKIN', '50ml', '2030-12-12', '162', '189', '205', '215', 0, 'COD00013'),
(14, 'WHITE BALANCE CLICK OXY-R', 'Sin Definir', 'MEDIK8 - Pigmentacio', 'PROSKIN', '2x10ml', '2030-12-12', '0', '0', '0', '0', 0, 'COD00014'),
(15, 'HYDR8 B5 SERUM', 'Sin Definir', 'MEDIK8 - Hidratante', 'PROSKIN', '30ml', '2030-12-12', '122', '142', '180', '190', 0, 'COD00015'),
(16, 'GLOW OIL', 'Sin Definir', 'MEDIK8 - Hidratante', 'PROSKIN', '30ml', '2030-12-12', '126', '148', '180', '190', 0, 'COD00016'),
(17, 'HYDR8 MANOS', 'Sin Definir', 'MEDIK8 - Hidratante', 'PROSKIN', '60ml', '2030-12-12', '72', '86', '95', '99', 0, 'COD00017'),
(18, 'LOPECIAN SHAMPOO', 'Sin Definir', 'LINEA CAPILAR', 'PROSKIN', '250ml', '2030-12-12', '88', '105', '128', '135', 0, 'COD00018'),
(19, 'PIROCTOL AS SHAMPOO', 'Sin Definir', 'LINEA CAPILAR', 'PROSKIN', '120ml', '2030-12-12', '49', '57', '75', '75', 0, 'COD00019'),
(20, 'BTSES FOCAL ACTION GEL (Inhibidor de arrugas)', 'Sin Definir', 'ANTIARRUGAS', 'SESDERMA', '15ml', '2030-12-12', '43', '66', '78', '83', 0, 'COD00020'),
(21, 'BTSES SERUM HIDRATANTE ANTIARRUGAS', 'Sin Definir', 'ANTIARRUGAS', 'SESDERMA', '30ml', '2030-12-12', '78', '120', '142', '150', 0, 'COD00021'),
(22, 'FILLDERMA ONE 50 ML. (Crema rellenadora)', 'Sin Definir', 'ANTIARRUGAS', 'SESDERMA', '50ml', '2030-12-12', '83', '128', '152', '160', 0, 'COD00022'),
(23, 'SESGEN 32 CREMA ACTIVADORA CELULAR', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '50ml', '2030-12-12', '105', '136', '155', '160', 0, 'COD00023'),
(24, 'SESGEN 32 SERUM ACTIVADOR CELULAR', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '30ml', '2030-12-12', '111', '144', '145', '165', 0, 'COD00024'),
(25, 'FACTOR G CREMA GEL REJUVENECEDOR', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '50ml', '2030-12-12', '89', '136', '162', '171', 0, 'COD00025'),
(26, 'FACTOR G CREMA REGENERADORA', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '50ml', '2030-12-12', '105', '130', '147', '155', 0, 'COD00026'),
(27, 'FACTOR G SERUM DE BURBUJAS LIPIDICAS', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '30ml', '2030-12-12', '111', '144', '145', '165', 0, 'COD00027'),
(28, 'RETI AGE CONTORNO DE OJOS ANTIAGING', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '15ml', '2030-12-12', '81', '106', '115', '125', 0, 'COD00028'),
(29, 'RETI AGE CREMA FACIAL ANTIAGING', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '50ml', '2030-12-12', '105', '136', '147', '155', 0, 'COD00029'),
(30, 'RETI AGE SERUM ANTIAGING', 'Sin Definir', 'GENOCOSMETICA', 'SESDERMA', '30ml', '2030-12-12', '111', '144', '145', '165', 0, 'COD00030'),
(31, 'C -VIT EYE CONTOUR CREAM 15 ML.', 'Sin Definir', 'ANTIOXIDANTES', 'SESDERMA', '15ml', '2030-12-12', '0', '0', '90', '100', 0, 'COD00031'),
(32, 'C-VIT CC CREAM SPF 15  30 ML.', 'Sin Definir', 'ANTIOXIDANTES', 'SESDERMA', '30ml', '2030-12-12', '72', '86', '100', '110', 0, 'COD00032'),
(33, 'RESVERADERM ANTIOX DNA REPAIR 50 ML.', 'Sin Definir', 'ANTIOXIDANTES', 'SESDERMA', '50ml', '2030-12-12', '98', '128', '150', '160', 0, 'COD00033'),
(34, 'RESVERADERM SERUM LIPOSOMADO', 'Sin Definir', 'ANTIOXIDANTES', 'SESDERMA', '30ml', '2030-12-12', '102', '160', '165', '0', 0, 'COD00034'),
(35, 'SESLASH KIT (SESLASH + GLICARE 15ml)', 'Sin Definir', 'CUIDADO DEL CONTORNO', 'SESDERMA', 'pack', '2030-12-12', '108', '112', '165', '180', 0, 'COD00035'),
(36, 'AZELAC RU LIPOSOMAL SERUM', 'Sin Definir', 'DESPIGMENTANTES', 'SESDERMA', '30ml', '2030-12-12', '0', '0', '0', '0', 0, 'COD00036'),
(37, 'REPASKIN DRY TOUCH (TOQUE SECO) SPF 50', 'Sin Definir', 'FOTOREPARACION', 'SESDERMA', '50 ml', '2030-12-12', '60', '72', '90', '100', 0, 'COD00037'),
(38, 'REPASKIN SLIK TOUCH (TACTO SEDA)SPF 50', 'Sin Definir', 'FOTOREPARACION', 'SESDERMA', '50ml', '2030-12-12', '68', '88', '100', '105', 0, 'COD00038'),
(39, 'HIDRADERM HYAL LIPOSOMAL SERUM ', 'Sin Definir', 'HIDRATACION', 'SESDERMA', '30ml', '2030-12-12', '78', '119', '143', '150', 0, 'COD00039'),
(40, 'HIDRADERM CLEANSING MILK 200 ML.', 'Sin Definir', 'EXFOLIACION Y LIMPIE', 'SESDERMA', '200ml', '2030-12-12', '54', '64', '75', '80', 0, 'COD00040'),
(41, 'AZELAC MOISTURIZING GEL  (GEL HIDRATANTE)50 ML.', 'Sin Definir', 'ROSACEA Y ACNE', 'SESDERMA', '50ml', '2030-12-12', '87', '104', '110', '120', 0, 'COD00041'),
(42, 'SALISES FOCAL TREATMENT 15 ML. ', 'Sin Definir', 'ROSACEA Y ACNE', 'SESDERMA', '15ml ', '2030-12-12', '39', '60', '71', '75', 0, 'COD00042'),
(43, 'SALISES MOISTURIZING GEL (GEL HIDRATANTE) 50 ML.', 'Sin Definir', 'ROSACEA Y ACNE', 'SESDERMA', '50ml', '2030-12-12', '89', '124', '147', '155', 0, 'COD00043'),
(44, 'KIT LUMINOSIDAD (C-VIT liposomal serum + C-VIT crema gel revitalizante)', 'Sin Definir', 'PACK PROMOCION', 'SESDERMA', 'pack (2 und)', '2030-12-12', '102', '136', '180', '190', 0, 'COD00044'),
(45, 'KIT C-VIT LUZ EN TU PIEL (Glowing fluid + eye contour cream)', 'Sin Definir', 'PACK PROMOCION', 'SESDERMA', 'pack (2 und)', '2030-12-12', '75', '112', '160', '170', 0, 'COD00045'),
(46, 'FOTO PROTECTOR SUN BRUSH MINERAL  FPS 50+ (Polvo Compacto)', 'Sin Definir', 'FACIALES', 'SESDERMA', '4gr', '2030-12-12', '64', '84', '95', '99', 0, 'COD00046'),
(47, 'FOTO ULTRA 100 ACTIVE UNIFY FUSION FLUID COLOR SPF 50+', 'Sin Definir', 'FACIALES', 'SESDERMA', '50ml', '2030-12-12', '78', '117', '110', '115', 0, 'COD00047'),
(48, 'FOTO ULTRA 100 SPOT PREVENT FUSION FLUID SPF 50+', 'Sin Definir', 'FACIALES', 'SESDERMA', '50ml', '2030-12-12', '75', '112', '105', '110', 0, 'COD00048'),
(49, 'FOTO PROTECTOR FUSION WATER  FPS 50+', 'Sin Definir', 'FACIALES', 'SESDERMA', '50ml', '2030-12-12', '66', '86', '95', '99', 0, 'COD00049'),
(50, 'ISDINCEUTICS FLAVO-C ( serum antioxidante con vitamina c y ginkgo biloba', 'Sin Definir', 'ISDINCEUTICS', 'SESDERMA', '30ml', '2030-12-12', '175', '175', '245', '249', 0, 'COD00050'),
(51, 'ISDINCEUTICS MELACLEAR (serum corrector unificador del tono)', 'Sin Definir', 'ISDINCEUTICS', 'SESDERMA', '15ml', '2030-12-12', '122', '122', '170', '174', 0, 'COD00051'),
(52, 'ISDINCEUTICS SKIN DROPS SAND (maquillaje fluido de cobertura adaptable)', 'Sin Definir', 'ISDINCEUTICS', 'SESDERMA', '15ml', '2030-12-12', '140', '140', '195', '199', 0, 'COD00052'),
(53, 'ISDINCEUTICS SKIN DROPS BRONZE  (maquillaje fluido de cobertura adaptable)', 'Sin Definir', 'ISDINCEUTICS', 'SESDERMA', '15ml', '2030-12-12', '140', '140', '195', '199', 0, 'COD00053'),
(54, 'ISDINCEUTICS K-OX EYES (crema contorno de ojos reduce bolsas y ojeras)', 'Sin Definir', 'ISDINCEUTICS', 'SESDERMA', '15gr', '2030-12-12', '129', '129', '180', '184', 0, 'COD00054'),
(55, 'FOAMING GLYCOLIC WASH', 'Sin Definir', 'NEOSTRATA', 'MEDSTYLE', 'Fco x 100ml', '2030-12-12', '75', '87', '100', '110', 0, 'COD00055'),
(56, 'OIL CONTROL GEL', 'Sin Definir', 'NEOSTRATA', 'MEDSTYLE', 'Fco x 30ml', '2030-12-12', '79', '68', '95', '100', 0, 'COD00056'),
(57, 'SKIN ACTIVE TRIPLE FIRMING NECK CREAM', 'Sin Definir', 'NEOSTRATA', 'MEDSTYLE', 'Fco x 75gr', '2030-12-12', '102', '127', '155', '160', 0, 'COD00057'),
(58, 'SKIN ACTIVE MATRIX SUPPORT SPF 30+', 'Sin Definir', 'NEOSTRATA', 'MEDSTYLE', 'Tubo x 50gr', '2030-12-12', '102', '127', '150', '160', 0, 'COD00058'),
(59, 'SKIN ACTIVE CELLULAR RESTORATION ', 'Sin Definir', 'NEOSTRATA', 'MEDSTYLE', 'Tubo x 50gr', '2030-12-12', '102', '127', '150', '160', 0, 'COD00059'),
(60, 'SKIN ACTIVE INTENSIVE EYE THERAPY', 'Sin Definir', 'NEOSTRATA', 'MEDSTYLE', 'Fco x 15gr', '2030-12-12', '95', '119', '140', '150', 0, 'COD00060'),
(61, 'SKIN ACTIVE EXFOLIATING WASH (Syner G Formula 8.5) 125 ML.', 'Sin Definir', 'NEOSTRATA', 'MEDSTYLE', 'Fco x 125ml', '2030-12-12', '83', '103', '120', '130', 0, 'COD00061'),
(62, 'SODERMIX', 'Sin Definir', 'LSI', 'MEDSTYLE', 'Tubo x 30gr', '2030-12-12', '79', '87', '100', '105', 0, 'COD00062'),
(63, 'POSTOPIX', 'Sin Definir', 'LSI', 'MEDSTYLE', 'Tubo x 15gr', '2030-12-12', '40', '56', '90', '100', 0, 'COD00063'),
(64, 'MD LASH FACTOR (Eye lash conditiones) ', 'Sin Definir', 'MD', 'MEDSTYLE', 'Tubo x 5.9ml', '2030-12-12', '300', '360', '390', '410', 0, 'COD00064'),
(65, 'COMPACTO SPF 50+', 'Sin Definir', 'MEDBLOCK', 'MEDSTYLE', '10gr', '2030-12-12', '47', '66', '80', '85', 0, 'COD00065'),
(66, 'CREMA COLOR SPF 50+', 'Sin Definir', 'MEDBLOCK', 'MEDSTYLE', 'Tubo x 80gr', '2030-12-12', '38', '55', '70', '75', 0, 'COD00066'),
(67, 'ATODERM INTENSIVE GEL MOUSSANT', 'Sin Definir', 'ATODERM', 'BIODERMA', '200ml', '2030-12-12', '0', '38', '60', '60', 0, 'COD00067'),
(68, 'ATODERM INTENSIVE BAUME', 'Sin Definir', 'ATODERM', 'BIODERMA', '200ml', '2030-12-12', '0', '71', '100', '110', 0, 'COD00068'),
(69, 'CICABIO CREMA 40 ML. ', 'Sin Definir', 'CICABIO', 'BIODERMA', '40ml', '2030-12-12', '45', '53', '76', '83', 0, 'COD00069'),
(70, 'HYDRABIO SERUM', 'Sin Definir', 'HYDRABIO', 'BIODERMA', '40ml', '2030-12-12', '0', '0', '0', '0', 0, 'COD00070'),
(71, 'PHOTODERM AKN MAT SPF 30 FLUIDE MATIFIANT', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '40ml', '2030-12-12', '61', '71', '110', '114', 0, 'COD00071'),
(72, 'PHOTODERM AKN SPF 30 SPRAY 100 ML. ', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '100ml', '2030-12-12', '0', '82', '110', '112', 0, 'COD00072'),
(73, 'PHOTODERM AR SPF 50+ TINTED CREAM NATURAL COLOUR', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '30ml', '2030-12-12', '61', '71', '110', '114', 0, 'COD00073'),
(74, 'PHOTODERM M SPF 50+ TINTED PROTECTIVE CREAM GOLDEN COLOUR', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '40ml', '2030-12-12', '61', '71', '110', '112', 0, 'COD00074'),
(75, 'PHOTODERM MAX SPF 50+ AQUA FLUIDE TEINTE LIGHT COLOUR', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '40ml', '2030-12-12', '0', '77', '105', '111', 0, 'COD00075'),
(76, 'PHOTODERM MAX SPF 50+ AQUA FLUIDE TEINTE GOLDEN COLOUR', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '40ml', '2030-12-12', '0', '77', '105', '111', 0, 'COD00076'),
(77, 'PHOTODERM NUDE TOUCH SPF 50+ NATURAL COLOUR', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '40ml', '2030-12-12', '67', '78', '115', '118', 0, 'COD00077'),
(78, 'PHOTODERM NUDE TOUCH SPF 50+ LIGHT COLOUR', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '40ml', '2030-12-12', '67', '78', '115', '118', 0, 'COD00078'),
(79, 'PHOTODERM NUDE TOUCH SPF 50+ GOLDEN  COLOUR', 'Sin Definir', 'PHOTODERM', 'BIODERMA', '40ml', '2030-12-12', '67', '78', '115', '118', 0, 'COD00079'),
(80, 'S?BIUM GEL MOUSSANT 500 ML.', 'Sin Definir', 'SEBIUM', 'BIODERMA', '500ml', '2030-12-12', '82', '96', '125', '130', 0, 'COD00080'),
(81, 'SEBIUM H2O SOLUTION MICELLAIRE 250 ML. ', 'Sin Definir', 'SEBIUM', 'BIODERMA', '250ml', '2030-12-12', '0', '52', '70', '75', 0, 'COD00081'),
(82, 'SEBIUM GLOBAL 30 ML.', 'Sin Definir', 'SEBIUM', 'BIODERMA', '30ml', '2030-12-12', '56', '56', '86', '90', 0, 'COD00082'),
(83, 'SEBIUM GLOBAL COVER (Teinte Universal)', 'Sin Definir', 'SEBIUM', 'BIODERMA', '30ml', '2030-12-12', '51', '59', '90', '95', 0, 'COD00083'),
(84, 'SENSIBIO AR SOIN ANTI - ROUGEURS 40ML.', 'Sin Definir', 'SENSIBIO', 'BIODERMA', '40ml', '2030-12-12', '0', '63', '95', '101', 0, 'COD00084'),
(85, 'SENSIBIO CONTORNO DE OJOS', 'Sin Definir', 'SENSIBIO', 'BIODERMA', '15ml', '2030-12-12', '0', '0', '0', '0', 0, 'COD00085'),
(86, 'SENSIBIO BB CREAM AR (clair light)', 'Sin Definir', 'SENSIBIO', 'BIODERMA', '40ml', '2030-12-12', '0', '94', '105', '110', 0, 'COD00086'),
(87, 'SENSIBIO H2O 100 ML. ', 'Sin Definir', 'SENSIBIO', 'BIODERMA', '100ml', '2030-12-12', '33', '39', '50', '52', 0, 'COD00087'),
(88, 'SENSIBIO H2O 500 ML. ', 'Sin Definir', 'SENSIBIO', 'BIODERMA', '250ml', '2030-12-12', '82', '73', '120', '124', 0, 'COD00088'),
(89, 'SENSIBIO GEL MOUSSANT 200 ML. ', 'Sin Definir', 'SENSIBIO', 'BIODERMA', '200ml', '2030-12-12', '50', '58', '83', '88', 0, 'COD00089'),
(90, 'PACK SENSIBIO BB CREAM AR (clair light) + ESPONJA DE APLICACI?N', 'Sin Definir', 'SENSIBIO', 'BIODERMA', 'pack', '2030-12-12', '0', '80', '120', '123', 0, 'COD00090'),
(91, 'KIT HIDRABIO (HIDRABIO SERUM 40 ml +SENSIBIO H2O SOLUTION MICELLAR 100ml+ SENSIBIO EYE CONTOUR 15ml)', 'Sin Definir', 'SENSIBIO', 'BIODERMA', 'pack', '2030-12-12', '0', '200', '219', '230', 0, 'COD00091'),
(92, 'PACK POST-PROCEDIMIENTO DERMATOLOGICO (Cicabio creme 40ml+Photoderm M spf 50+ golden colour 40ml', 'Sin Definir', 'SENSIBIO', 'BIODERMA', 'pack', '2030-12-12', '0', '96', '0', '0', 0, 'COD00092'),
(93, 'TECNOSKIN LIPBOOST GLOSS COLOR (Hyaluronic acid)', 'Sin Definir', 'FISSION LAB', 'FISION LAB', '7ml', '2030-12-12', '46', '50', '57', '60', 0, 'COD00093'),
(94, 'TECNOSKIN LIPBOOST GLOSS WHITE (Hyaluronic acid)', 'Sin Definir', 'FISSION LAB', 'FISION LAB', '7ml', '2030-12-12', '46', '50', '57', '60', 0, 'COD00094'),
(95, 'STRATAMED (Tratamiento de Cicatrices)', 'Sin Definir', 'FISSION LAB', 'FISION LAB', '10gr', '2030-12-12', '95', '114', '124', '130', 0, 'COD00095'),
(96, 'AMINOTER MAX SHAMPOO', 'Sin Definir', 'LAFAGE', 'LAFAGE', '300ml', '2030-12-12', '69', '76', '100', '110', 0, 'COD00096'),
(97, 'AMINOTER (Suplemento dietario) 30 Capsulas x caja', 'Sin Definir', 'LAFAGE', 'LAFAGE', 'cap 30', '2030-12-12', '0', '161', '235', '239', 0, 'COD00097'),
(98, 'C-VIT CREMA HIDRO PROTECTORA (PIEL NORMAL Y SECA) ', 'Sin Definir', 'CVITAL', 'ATACHE (EUROLABS)', '50ml', '2030-12-12', '0', '69', '140', '150', 0, 'COD00098'),
(99, 'C-VIT CREMA HIDRO PROTECTORA (PIELGRASA Y MIXTA) ', 'Sin Definir', 'CVITAL', 'ATACHE (EUROLABS)', '50ml', '2030-12-12', '0', '69', '140', '150', 0, 'COD00099'),
(100, 'INTENSIVE LIFT CONTOUR (contorno de ojos y labios) ', 'Sin Definir', 'LIFT THERAPY', 'ATACHE (EUROLABS)', '15ml', '2030-12-12', '0', '129', '170', '180', 0, 'COD00100'),
(101, 'PERFORMANCE NECK SOLUTION (REAFIRMANTE DE CUELLO Y PAPADA) ', 'Sin Definir', 'LIFT THERAPY', 'ATACHE (EUROLABS)', '50ml', '2030-12-12', '0', '175', '220', '230', 0, 'COD00101'),
(102, 'PERFORMANCE SOLUTION (SERUM ACLARADOR EPID?RMICO)  ', 'Sin Definir', 'DESPIGMEN', 'ATACHE (EUROLABS)', '50ml', '2030-12-12', '0', '175', '220', '230', 0, 'COD00102'),
(103, 'SPECIFIC ANTI-TACHES SOLUTION (CREMA DESPIGMENTANTE DERMICO) ', 'Sin Definir', 'DESPIGMEN', 'ATACHE (EUROLABS)', '15ml', '2030-12-12', '0', '99', '128', '135', 0, 'COD00103'),
(104, 'UMBRELLA BASE COMPACTA SPF 50+ TONO OSCURO', 'Sin Definir', 'UMBRELLA', 'ROEMMERS', '11g', '2030-12-12', '83', '83', '91', '100', 0, 'COD00104'),
(105, 'UMBRELLA BASE COMPACTA SPF 50+ TONO CLARO', 'Sin Definir', 'UMBRELLA', 'ROEMMERS', '11g', '2030-12-12', '83', '83', '91', '100', 0, 'COD00105'),
(106, 'UMBRELLA GEL SPF 50+ (piel normal a grasa)', 'Sin Definir', 'UMBRELLA', 'ROEMMERS', '60gr', '2030-12-12', '73', '73', '90', '95', 0, 'COD00106'),
(107, 'RESVERAX CREMA FACIAL ANTIAGE 30 G.', 'Sin Definir', 'HIDRISAGE', 'ROEMMERS', '30gr', '2030-12-12', '75', '75', '137', '140', 0, 'COD00107'),
(108, 'PLIANCE BASE COMPACTA TONO CLARO 11 G.', 'Sin Definir', 'HIDRISAGE', 'ROEMMERS', '11gr', '2030-12-12', '76', '76', '135', '140', 0, 'COD00108'),
(109, 'AQUAPRIM CREMA HIDRATANTE 30G. ', 'Sin Definir', 'HIDRISAGE', 'ROEMMERS', '30gr', '2030-12-12', '75', '75', '137', '140', 0, 'COD00109'),
(110, 'LUMED EMULGEL (Aclarante despigmentante) 40 G. PROTECTOR SOLAR SPF 50+', 'Sin Definir', 'HIDRISAGE', 'ROEMMERS', '40gr', '2030-12-12', '0', '50', '102', '110', 0, 'COD00110'),
(111, 'LUMED BASE FLUIDA (Aclarante despigmentante)PROTECTOR SOLAR SPF 50+ TONO CLARO 40 G.', 'Sin Definir', 'HIDRISAGE', 'ROEMMERS', '40gr', '2030-12-12', '83', '83', '145', '150', 0, 'COD00111'),
(112, 'LUMED BASE FLUIDA (Aclarante despigmentante)PROTECTOR SOLAR SPF 50+ TONO OSCURO 40 G.', 'Sin Definir', 'HIDRISAGE', 'ROEMMERS', '40gr', '2030-12-12', '83', '83', '150', '154', 0, 'COD00112'),
(113, 'CAVIAR LUXURY (CONTORNO DE OJOS)', 'Sin Definir', 'LE LAB', 'LE LAB', '30gr.', '2030-12-12', '0', '0', '100', '120', 0, 'COD00113'),
(114, 'CAVIAR LUXURY CREMA', 'Sin Definir', 'LE LAB', 'LE LAB', '50gr.', '2030-12-12', '0', '0', '150', '160', 0, 'COD00114'),
(115, 'CREMA RESTAURADORA ESPECIAL DE MANOS', 'Sin Definir', 'LE LAB', 'LE LAB', '50gr.', '2030-12-12', '0', '0', '0', '0', 0, 'COD00115'),
(116, 'GLOBAL PERFECTION CREMA (CARA CUELLO Y ESCOTE)', 'Sin Definir', 'LE LAB', 'LE LAB', '50gr', '2030-12-12', '80', '100', '0', '0', 0, 'COD00116'),
(117, 'MICRO ESFERAS DE VITAMINA C EN GEL DE ACIDO HIALURONICO ', 'Sin Definir', 'LE LAB', 'LE LAB', '50gr.', '2030-12-12', '70', '90', '130', '140', 0, 'COD00117'),
(118, 'MOLECULAR EXPERT YOUNGER SKIN', 'Sin Definir', 'LE LAB', 'LE LAB', '50gr.', '2030-12-12', '80', '100', '0', '0', 0, 'COD00118'),
(119, 'BLANCHE EFFECT CREMA DE NOCHE', 'Sin Definir', 'LE LAB', 'LE LAB', '30gr', '2030-12-12', '0', '0', '0', '0', 0, 'COD00119'),
(120, 'CREMA DESPIGMENTANTE CON AC. TRANEXAMICO', 'Sin Definir', 'LE LAB', 'LE LAB', '30ng', '2030-12-12', '36', '56', '0', '0', 0, 'COD00120'),
(121, 'BEBE GEL DERMOLIMPIADOR', 'Sin Definir', 'ALPHANOVA', 'ALPHANOVA', '200ml', '2030-12-12', '0', '0', '70', '80', 0, 'COD00121'),
(122, 'BEBE COLD CREAM', 'Sin Definir', 'ALPHANOVA', 'ALPHANOVA', '50gr', '2030-12-12', '0', '0', '70', '80', 0, 'COD00122'),
(123, 'SUN MILK (BLOQUEADOR)', 'Sin Definir', 'ALPHANOVA', 'ALPHANOVA', '50gr', '2030-12-12', '0', '0', '0', '0', 0, 'COD00123'),
(124, 'FEQOXIL BUCODISPENSABLE (PASTILLAS)', 'Sin Definir', 'OTROS', 'OTROS', 'cap 30', '2030-12-12', '0', '0', '0', '0', 0, 'COD00124'),
(125, 'TONICO ASTRINGENTE', 'Sin Definir', 'EXEL', 'EXCEL', '0', '2030-12-12', '0', '0', '50', '60', 0, 'COD00125'),
(126, 'TONICO COLAGENO ', 'Sin Definir', 'EXEL', 'EXCEL', '0', '2030-12-12', '0', '0', '50', '60', 0, 'COD00126'),
(127, 'Crema Antiacne2', 'Asepsia', 'Belleza', 'Portugal', '100 ml', '2000-12-10', '111', '222', '333', '444', 3, 'COD00127'),
(129, 'crema hinds', 'yanbal', 'cremas', 'portugal', 'frasco x 100ml', '2020-10-01', '1043', '12', '13', '16', 50, '43S11010156'),
(130, '', '', '', '', '', '0000-00-00', '0', '0', '0', '0', 0, ''),
(131, '', '', '', '', '', '0000-00-00', '0', '0', '0', '0', 0, ''),
(132, 'Crema Hidratante 13', 'Desconocido', 'Belleza', 'Natura', '100 ml', '2000-09-11', '1', '2', '3', '4', 3, 'COD00129'),
(133, 'Crema Hidratante14', 'Panadol', 'Belleza', 'Portugal', '100 ml', '2000-12-10', '111', '222', '333', '444', 3, 'COD00131'),
(134, 'Crema Hidratante14', 'Panadol', 'Belleza', 'Portugal', '100 ml', '2000-12-10', '111', '222', '333', '444', 3, 'COD00131'),
(135, 'Crema Hidratante25', 'Panadol', 'Belleza', 'Portugal', '100 ml', '2000-12-10', '111', '222', '333', '444', 3, '46464646'),
(136, 'Crema Hidratante14', 'Panadol', 'Belleza', 'Portugal', '100 ml', '2000-12-10', '111', '222', '333', '444', 3, 'COD00131'),
(137, 'Crema Hidratante14', 'Panadol', 'Belleza', 'Portugal', '100 ml', '2000-12-10', '111', '222', '333', '444', 3, 'COD00131'),
(138, 'Crema Hidratante 39', 'Panadol', 'Belleza', 'Portugal', '100 ml', '2019-07-18', '111', '222', '333', '444', 3, 'COD00139'),
(139, 'Crema prueba', 'natura', 'crema corporal', 'brasil', 'cja x 100 ml', '2018-12-27', '45', '47', '45', '50', 100, '45128945df12dffdf'),
(140, 'Crema Hidratante46', 'Panadol', 'Belleza', 'Portugal', '100 ml', '2018-09-14', '111', '222', '333', '444', 3, 'COD0000146'),
(141, 'Crema Hidratante150', 'Panadol', 'Belleza', 'Portugal', '100 ml', '2018-09-14', '111', '222', '333', '444', 3, 'COD00150'),
(142, 'GENTLECLEANSE9', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '150ml', '2030-12-12', '0', '0', '0', '0', 0, 'COD00001'),
(143, 'Crema Hidratante157', 'Panadol2', 'Belleza', 'Portugal', '100 ml', '2018-09-17', '111', '222', '333', '444', 3, 'COD0000157'),
(144, 'crema ponds', 'unique', 'belleza', 'unique', 'frasco x 100ml', '2019-02-02', '10', '12', '14', '16', 20, '3435465'),
(145, 'Crema Hidratante 160', 'Panadol', 'Belleza', 'Portugal', '100 ml', '2018-09-17', '111', '222', '333', '444', 3, 'COD0000160'),
(146, 'GENTLECLEANSE9', 'Sin Definir', 'MEDIK8 - Antiageing', 'PROSKIN', '150ml', '2030-12-12', '0', '0', '0', '0', 0, 'COD00001'),
(147, 'Crema Hidratante 161', 'Panadol', 'Belleza', 'Portugal', '100 ml', '2018-09-19', '111', '222', '333', '444', 3, 'COD00161'),
(148, 'Crema Hidratante 163', 'Panadol', 'Belleza', 'Portugal', '100 ml', '2018-09-24', '111', '222', '333', '444', 3, 'COD00163'),
(149, 'Crema Hidratante 164', 'Panadol', 'Belleza', 'Portugal', '100 ml', '2018-09-24', '111', '222', '333', '444', 3, 'COD0000164'),
(150, 'Crema hidratante 168', 'Panadol', 'Belleza', 'Portugal', '100 ml', '2018-09-27', '111', '222', '333', '444', 3, 'COD0000168');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `idproveedor` int(11) NOT NULL,
  `descripcionproveedor` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`idproveedor`, `descripcionproveedor`) VALUES
(1, 'Jc Distribuciones'),
(3, 'RyR distribuciones'),
(4, 'MyM Distribuciones'),
(8, 'HyTDistribuciones'),
(11, 'lucas');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registroventa`
--

CREATE TABLE `registroventa` (
  `idregistroventa` int(11) NOT NULL,
  `codigoclienterv` int(30) NOT NULL,
  `descripcionclienterv` varchar(100) NOT NULL,
  `codigoproductorv` varchar(50) NOT NULL,
  `descripcionproductorv` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `registroventa`
--

INSERT INTO `registroventa` (`idregistroventa`, `codigoclienterv`, `descripcionclienterv`, `codigoproductorv`, `descripcionproductorv`) VALUES
(5, 55550000, 'Hernan Cama', 'COD00010', 'BETA GEL '),
(8, 33330000, 'Pedro Mamani', 'COD00009', 'BETA CLEANSE'),
(12, 55550000, 'Hernan Cama', 'COD00004', 'EYE CLEANSE'),
(13, 11110000, 'Juan Quispe', 'COD00016', 'GLOW OIL'),
(14, 22220000, 'Osvaldo Arapa', 'COD00006', 'RETINOL 1TR CREAM'),
(15, 44440000, 'Juan Perez', 'COD00014', 'WHITE BALANCE CLICK OXY-R'),
(16, 33330000, 'Pedro Mamani', 'COD00123', 'SUN MILK (BLOQUEADOR)'),
(28, 0, 'Juan Quispe', 'COD00010', 'BETA GEL '),
(30, 0, 'Vanesa Sulla', 'COD00001', 'GENTLECLEANSE5'),
(31, 0, 'Hernan Cama', 'COD00044', 'KIT LUMINOSIDAD (C-VIT liposomal serum + C-VIT crema gel revitalizante)'),
(32, 0, '', '', ''),
(33, 0, 'Hernan Cama', 'COD00044', 'KIT LUMINOSIDAD (C-VIT liposomal serum + C-VIT crema gel revitalizante)'),
(34, 0, '', 'COD00004', 'EYE CLEANSE');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `idusuario` int(5) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `contrasena` varchar(20) NOT NULL,
  `tipo` int(1) NOT NULL DEFAULT 2,
  `descripcionusuario` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`idusuario`, `usuario`, `contrasena`, `tipo`, `descripcionusuario`) VALUES
(1, 'admin', 'admin', 1, 'Gerente General'),
(2, 'hernan', 'hernan', 2, 'Hernan Cama'),
(3, '', '', 2, 'Javier Wong Hut'),
(5, '', '', 2, 'marco');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `usuarioID` int(5) NOT NULL,
  `usuario` varchar(20) NOT NULL,
  `contrasena` varchar(20) NOT NULL,
  `tipo` int(1) NOT NULL DEFAULT 2
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`usuarioID`, `usuario`, `contrasena`, `tipo`) VALUES
(1, 'admin', 'admin', 1),
(2, 'hernan', 'hernan', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `almacen`
--
ALTER TABLE `almacen`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idcliente`);

--
-- Indices de la tabla `compraventa`
--
ALTER TABLE `compraventa`
  ADD PRIMARY KEY (`codigocompraventa`);

--
-- Indices de la tabla `familia`
--
ALTER TABLE `familia`
  ADD PRIMARY KEY (`idfamilia`);

--
-- Indices de la tabla `laboratorio`
--
ALTER TABLE `laboratorio`
  ADD PRIMARY KEY (`idlaboratorio`);

--
-- Indices de la tabla `marca`
--
ALTER TABLE `marca`
  ADD PRIMARY KEY (`idmarca`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idproducto`);

--
-- Indices de la tabla `producto2`
--
ALTER TABLE `producto2`
  ADD PRIMARY KEY (`idproducto`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`idproducto`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idproveedor`);

--
-- Indices de la tabla `registroventa`
--
ALTER TABLE `registroventa`
  ADD PRIMARY KEY (`idregistroventa`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`idusuario`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`usuarioID`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `almacen`
--
ALTER TABLE `almacen`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idcliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT de la tabla `compraventa`
--
ALTER TABLE `compraventa`
  MODIFY `codigocompraventa` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT de la tabla `familia`
--
ALTER TABLE `familia`
  MODIFY `idfamilia` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `laboratorio`
--
ALTER TABLE `laboratorio`
  MODIFY `idlaboratorio` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `marca`
--
ALTER TABLE `marca`
  MODIFY `idmarca` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `idproducto` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;

--
-- AUTO_INCREMENT de la tabla `producto2`
--
ALTER TABLE `producto2`
  MODIFY `idproducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `productos`
--
ALTER TABLE `productos`
  MODIFY `idproducto` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idproveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `registroventa`
--
ALTER TABLE `registroventa`
  MODIFY `idregistroventa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT de la tabla `usuario`
--
ALTER TABLE `usuario`
  MODIFY `idusuario` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `usuarioID` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
