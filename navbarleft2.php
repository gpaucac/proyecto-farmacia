<!DOCTYPE html>

  <nav class="navbar navbar-inverse navbar-fixed-left">
    <div class="container">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="../admin.php">Sistema de Ventas</a>
          </div>

          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="#"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Compra</a></li>
              <li><a href="#"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Venta</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Productos <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="../producto/registroproducto1.php">Agregar Producto</a></li>
                  <li><a href="../marca/registromarca1.php">Agregar Marca</a></li>
                  <li><a href="../familia/registrofamilia1.php">Agregar Familia</a></li>
                  <li><a href="../laboratorio/registrolaboratorio1.php">Agregar Laboratorio</a></li>
                </ul>
              </li>
             <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Administrador <span class="caret"></span></a>
                <ul class="dropdown-menu">
                 <li><a href="../usuario/registrousuario1.php">Agregar Usuarios</a></li>
                  <li><a href="../cliente/registrocliente1.php">Agregar Cliente</a></li>
                  <li><a href="../proveedor/registroproveedor1.php">Agregar Proveedor</a></li>
                </ul>
              </li>
              <li><a href="#"><span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Reportes</a></li>
            </ul>
          </div>
    </div>
  </nav>

  <div class="container">
  </div>